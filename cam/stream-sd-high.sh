#!/bin/bash

set +x

raspivid -fl -t 0 -w 960 -h 540 -fps 25 -b 1800000 -p 0,0,640,480 -o - | gst-launch -v fdsrc ! h264parse ! rtph264pay config-interval=1 pt=96 ! gdppay ! tcpserversink host=0.0.0.0 port=5000
