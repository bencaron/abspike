package main

import (
	"fmt"

	"github.com/bencaron/pvcpike/motion"
	"github.com/hybridgroup/gobot"
	"github.com/hybridgroup/gobot/platforms/joystick"
	"github.com/hybridgroup/gobot/platforms/mqtt"
)

func main() {
	gbot := gobot.NewGobot()

	mq := mqtt.NewMqttAdaptor("server", "tcp://localhost:1883", "control")
	joystickAdaptor := joystick.NewJoystickAdaptor("ps3")
	joystick := joystick.NewJoystickDriver(joystickAdaptor,
		"ps3",
		"./dualshock3.json",
	)

	work := func() {
		rstick := stick{x: 0, y: 0}
		lstick := stick{x: 0, y: 0}
		// READ http://blog.hypersect.com/interpreting-analog-sticks/
		gobot.On(joystick.Event("left_y"), func(data interface{}) {
			val := getInt8(data)
			fmt.Printf("read     ly=%d\n", val)
			if lstick.y != val {
				lstick.y = val
				fmt.Printf("    send ly=%d\n", motion.Stick2Power(lstick.y))
				mq.Publish("motor/left", motion.Stick2Power(lstick.y))
			}
			mq.Publish("motor/left", motion.Stick2Power(lstick.y))
		})

		gobot.On(joystick.Event("right_y"), func(data interface{}) {
			val := getInt8(data)
			fmt.Printf("read ry=%d\n", val)
			if rstick.y != val {
				rstick.y = val
				mq.Publish("motor/right", motion.Stick2Power(rstick.y))
			}
		})

		/** UP/Down **/
		gobot.On(joystick.Event("up_press"), func(data interface{}) {
			fmt.Printf("up_press fullForward %d\n", fullForward)
			mq.Publish("motor/up", motion.Stick2Power(fullForward))
		})

		gobot.On(joystick.Event("down_press"), func(data interface{}) {
			fmt.Printf("down_press backward %d\n", fullReverse)
			mq.Publish("motor/up", motion.Stick2Power(fullReverse))
		})

		gobot.On(joystick.Event("up_release"), func(data interface{}) {
			fmt.Println("up_release")
			mq.Publish("motor/up", motion.Stick2Power(0))
		})

		gobot.On(joystick.Event("down_release"), func(data interface{}) {
			fmt.Println("down_release")
			mq.Publish("motor/up", motion.Stick2Power(0))
		})
		/******
		 * Square, Triangle and strange buttons
		 ******/

		gobot.On(joystick.Event("square_press"), func(data interface{}) {
			fmt.Println("square_press")
		})
		gobot.On(joystick.Event("square_release"), func(data interface{}) {
			fmt.Println("square_release")
		})
		gobot.On(joystick.Event("triangle_press"), func(data interface{}) {
			fmt.Println("triangle_press")
		})
		gobot.On(joystick.Event("triangle_release"), func(data interface{}) {
			fmt.Println("triangle_release")
		})
	}

	robot := gobot.NewRobot("joystickBot",
		[]gobot.Connection{joystickAdaptor, mq},
		[]gobot.Device{joystick},
		work,
	)
	gbot.AddRobot(robot)
	gbot.Start()
}
