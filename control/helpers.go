package main

import "fmt"

// http://blog.hypersect.com/interpreting-analog-sticks/
func getInt8(data interface{}) int8 {
	out, ok := data.(int16)
	fmt.Printf("getInt8 read data2int16=>%d\n", out)
	if ok {
		fmt.Printf("   getInt8 return=>%d\n", out>>8)
		return int8(out >> 8)
	}
	fmt.Println("stick read error, can't cast NOT OK?, Forcing a 0 value")
	return 0
}
