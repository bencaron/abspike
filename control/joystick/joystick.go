package main

import (
	"fmt"

	"github.com/bencaron/pvcpike/motion"
	"github.com/hybridgroup/gobot"
	"github.com/hybridgroup/gobot/platforms/joystick"
	"github.com/hybridgroup/gobot/platforms/mqtt"
)

func main2() {
	gbot := gobot.NewGobot()

	mq := mqtt.NewMqttAdaptor("server", "tcp://localhost:1883", "control")
	joystickAdaptor := joystick.NewJoystickAdaptor("ps3")
	joystick := joystick.NewJoystickDriver(joystickAdaptor,
		"ps3",
		"./dualshock3.json",
	)

	work := func() {
		motors := motion.MotorsPower{}
		rstick := stick{x: 0, y: 0}
		// READ http://blog.hypersect.com/interpreting-analog-sticks/
		gobot.On(joystick.Event("left_y"), func(data interface{}) {
			fmt.Println("FIXME incomplet left_y", data)
			motors.Sky = getInt8(data)
			publish(mq, "move/vertical", motors)
		})

		gobot.On(joystick.Event("right_x"), func(data interface{}) {
			val := getInt8(data)
			fmt.Printf("read x=%d", val)
			if rstick.x != val {
				rstick.x = val
				publish(mq, "move/setvalue", stick2motors(rstick, motors))
			}
		})
		gobot.On(joystick.Event("right_y"), func(data interface{}) {
			val := getInt8(data)
			fmt.Printf("read y=%d", val)
			if rstick.y != val {
				rstick.y = val
				publish(mq, "move/setvalue", stick2motors(rstick, motors))
			}
		})

		/******
		 * Square, Triangle and strange buttons
		 ******/

		gobot.On(joystick.Event("square_press"), func(data interface{}) {
			fmt.Println("square_press")
		})
		gobot.On(joystick.Event("square_release"), func(data interface{}) {
			fmt.Println("square_release")
		})
		gobot.On(joystick.Event("triangle_press"), func(data interface{}) {
			fmt.Println("triangle_press")
		})
		gobot.On(joystick.Event("triangle_release"), func(data interface{}) {
			fmt.Println("triangle_release")
		})
	}

	/*
	   data := []byte("")
	   gobot.Every(1*time.Second, func() {
	   	mq.Publish("lights/on", data)
	   })
	   gobot.Every(2*time.Second, func() {
	   	mq.Publish("lights/off", data)
	   })
	*/

	robot := gobot.NewRobot("joystickBot",
		[]gobot.Connection{joystickAdaptor, mq},
		[]gobot.Device{joystick},
		work,
	)
	gbot.AddRobot(robot)
	gbot.Start()
}
