package main

import (
	"testing"

	"github.com/bencaron/pvcpike/motion"
	"github.com/stretchr/testify/assert"
)

func TestStick2MotorsFullFront(t *testing.T) {
	inputStick := stick{x: 0, y: 127}
	inputMotor := motion.MotorsPower{
		Babord:  50,
		Tribord: -25,
		Sky:     10,
	}
	expectedMotor := motion.MotorsPower{
		Babord:  127,
		Tribord: 127,
		Sky:     10,
	}

	assert.Equal(t,
		stick2motors(inputStick, inputMotor),
		expectedMotor)
}

func TestStick2MotorsFullTribord(t *testing.T) {
	inputStick := stick{x: 127, y: 0}
	inputMotor := motion.MotorsPower{
		Babord:  50,
		Tribord: -25,
		Sky:     20,
	}
	expectedMotor := motion.MotorsPower{
		Babord:  127,
		Tribord: -127,
		Sky:     20,
	}

	assert.Equal(t,
		stick2motors(inputStick, inputMotor),
		expectedMotor)
}

func TestStick2MotorsFullBabord(t *testing.T) {
	inputStick := stick{x: -127, y: 0}
	inputMotor := motion.MotorsPower{
		Babord:  50,
		Tribord: -25,
		Sky:     20,
	}
	expectedMotor := motion.MotorsPower{
		Babord:  -127,
		Tribord: 127,
		Sky:     20,
	}

	assert.Equal(t,
		stick2motors(inputStick, inputMotor),
		expectedMotor)
}

func TestStick2MotorsDiago45Tribord(t *testing.T) {
	inputStick := stick{x: 62, y: 62}
	inputMotor := motion.MotorsPower{
		Babord:  50,
		Tribord: -25,
		Sky:     20,
	}
	expectedMotor := motion.MotorsPower{
		Babord:  62,
		Tribord: 0,
		Sky:     20,
	}

	assert.Equal(t,
		stick2motors(inputStick, inputMotor),
		expectedMotor)
}

func TestStick2MotorsDiago75Tribord(t *testing.T) {
	inputStick := stick{x: 22, y: 100}
	inputMotor := motion.MotorsPower{
		Babord:  50,
		Tribord: -25,
		Sky:     20,
	}
	expectedMotor := motion.MotorsPower{
		Babord:  100,
		Tribord: 78,
		Sky:     20,
	}

	assert.Equal(t,
		stick2motors(inputStick, inputMotor),
		expectedMotor)
}
