package main

import (
	"fmt"
	"os"

	"github.com/hybridgroup/gobot"
	"github.com/hybridgroup/gobot/platforms/mqtt"
)

func main() {
	gbot := gobot.NewGobot()

	//mqttAdaptor := mqtt.NewMqttAdaptor("server", "tcp://localhost:1883", "control_tester")
	mqttAdaptor := mqtt.NewMqttAdaptor("server", os.Args[1], "control_tester")

	work := func() {

		mqttAdaptor.On("motor/left", func(data []byte) {
			value := data[0]
			fmt.Printf("motor left ; data %d\n", value)
		})
		mqttAdaptor.On("motor/right", func(data []byte) {
			value := data[0]
			fmt.Printf("motor righ; data %d\n", value)
		})
		mqttAdaptor.On("motor/up", func(data []byte) {
			value := data[0]
			fmt.Printf("motor up; data %d\n", value)
		})
		mqttAdaptor.On("move/backward", func(data []byte) {
			fmt.Printf("moving backward; data %s\n", data)
			value := data[0]
			fmt.Printf("moving backward; data %d\n", value)
		})
		mqttAdaptor.On("move/setvalue", func(data []byte) {
			fmt.Printf("moving setvalue %d\n", data)
		})
		mqttAdaptor.On("move/vertical", func(data []byte) {
			fmt.Printf("moving vertical %d\n", data)
		})
	}
	robot := gobot.NewRobot("control_tester",
		[]gobot.Connection{mqttAdaptor},
		work,
	)

	gbot.AddRobot(robot)

	gbot.Start()
}
