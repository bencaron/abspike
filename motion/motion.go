package motion

import "github.com/hybridgroup/gobot/platforms/firmata"

// probablement pas necessaire, utiliser seulement dans sub/sub.go

// AdaptorXX setup the adapter for control of the sub motions
func AdaptorXX(device string) (*firmata.FirmataAdaptor, error) {
	firmataAdaptor := firmata.NewFirmataAdaptor("arduino", device)

	return firmataAdaptor, nil
}
