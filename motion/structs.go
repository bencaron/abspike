package motion

import "fmt"

// Center is the value to consider the center of the byte sent
const Center uint8 = 128

// CenterMargin is the how big the buffer around the center to use to normalize
// the stick value so we can flatten the value received to compensate for bad controllers
const CenterMargin uint8 = 10

// CenterizePower take a int and fit in in a byte value Centered around the Center point
func CenterizePower(in int8) byte {
	return byte(uint8(in) + Center)
}

// UnCenterizePower take a byte from the wire and convert to a signed int value
func UnCenterizePower(in byte) int8 {
	return int8(in - Center)
}

// Stick2Power Convert a value received from a stick to a byte array suitable for MQTT transport
func Stick2Power(val int8) []byte {
	return []byte{
		CenterizePower(val),
	}
}

// Forward direction
const Forward int8 = 1

// Backward direction
const Backward int8 = 2

// Stop mean direction == stop
const Stop int8 = 0

// Direction read the byte value transmitted on the wire and figure what direction
// the motor should go according to it's position around the motion.Center
func Direction(value byte) int8 {
	fmt.Printf("Direction for value=%d ", value)
	if value > (Center + CenterMargin) {
		fmt.Printf(" %d > (center %d + centerMargin%d =%d)\n", value, Center, CenterMargin, Center+CenterMargin)
		return Backward
	} else if value < (Center - CenterMargin) {
		fmt.Printf("  %d < (center %d + centerMargin%d =%d)\n", value, Center, CenterMargin, Center+CenterMargin)
		return Forward
	} else {
		fmt.Printf("Direction for value=%d = stop ", value)
		return Stop
	}
}
