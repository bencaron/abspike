package motion

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMotors2Bytes(t *testing.T) {
	motor := MotorsPower{
		Babord:  12,
		Tribord: -15,
		Sky:     10,
	}
	expectedBytes := []byte{byte(140), byte(113), byte(138)}
	assert.Equal(t, expectedBytes, Motors2Bytes(motor))
}

func TestBytes2Motors(t *testing.T) {
	thebyteslice := []byte{byte(116), byte(173), byte(148)}
	expectedMotor := MotorsPower{
		Babord:  -12,
		Tribord: 45,
		Sky:     20,
	}
	assert.Equal(t, expectedMotor, Bytes2Motors(thebyteslice))
}
