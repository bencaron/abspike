#!/bin/bash


DATADIR=`pwd`/data/mqtt/data/
CONFDIR=`pwd`/data/mqtt/conf/
LOGDIR=`pwd`/data/mqtt/log/

mkdir -p $DATADIR
mkdir -p $CONFDIR
mkdir -p $LOGDIR
# place your mosquitto.conf in /srv/mqtt/config/
# NOTE: You have to change the permissions of the directories
# to allow the user to read/write to data and log and read from
# config directory
# For TESTING purposes you can use chmod -R 777 /srv/mqtt/*
# Better use "-u" with a valid user id on your docker host

docker run -d -p 1883:1883 -p 9001:9001 \
-v $CONFDIR:/mqtt/config:ro \
-v $LOGDIR:/mqtt/log \
-v $DATADIR/:/mqtt/data/ \
--name mqtt toke/mosquitto
