package main

import (
	"fmt"
	"os"

	"github.com/bencaron/pvcpike/motion"
	"github.com/hybridgroup/gobot"
	"github.com/hybridgroup/gobot/platforms/firmata"
	"github.com/hybridgroup/gobot/platforms/gpio"
	"github.com/hybridgroup/gobot/platforms/mqtt"
)

const center uint8 = 128

func main() {
	gbot := gobot.NewGobot()

	//mqttAdaptor := mqtt.NewMqttAdaptor("server", "tcp://localhost:1883", "control_tester")
	mqttAdaptor := mqtt.NewMqttAdaptor("server", os.Args[1], "sub_controller")
	firmataAdaptor := firmata.NewFirmataAdaptor("arduino", "/dev/ttyUSB0")
	//led := gpio.NewLedDriver(firmataAdaptor, "led", "13")
	motorLeft := gpio.NewMotorDriver(firmataAdaptor, "motorLeft", "10")
	motorLeft.ForwardPin = "8"
	motorLeft.BackwardPin = "9"
	motorRight := gpio.NewMotorDriver(firmataAdaptor, "motorRight", "6")
	motorRight.ForwardPin = "7"
	motorRight.BackwardPin = "12"
	// FIXME pin 3 not needed
	motorUp := gpio.NewMotorDriver(firmataAdaptor, "motorUp", "3")
	motorUp.ForwardPin = "2"
	motorUp.BackwardPin = "4"

	work := func() {
		mqttAdaptor.On("motor/left", func(data []byte) {
			fmt.Printf("motor left; data %d\n", data[0])
			move(motorLeft, data[0])
		})
		mqttAdaptor.On("motor/right", func(data []byte) {
			fmt.Printf("motor left; data %d\n", data[0])
			move(motorRight, data[0])
		})
		mqttAdaptor.On("motor/up", func(data []byte) {
			fmt.Printf("motor up; data %d\n", data[0])
			move(motorUp, data[0])
		})
		mqttAdaptor.On("move/backward", func(data []byte) {
			fmt.Printf("moving backward; data %s\n", data)
			value := data[0]
			fmt.Printf("moving backward; data %d\n", value)
		})
		mqttAdaptor.On("move/setvalue", func(data []byte) {
			fmt.Printf("moving setvalue %d\n", data)
		})
		mqttAdaptor.On("move/vertical", func(data []byte) {
			fmt.Printf("moving vertical %d\n", data)
		})
	}
	robot := gobot.NewRobot("sub",
		[]gobot.Connection{mqttAdaptor, firmataAdaptor},
		[]gobot.Device{motorLeft, motorRight, motorUp},
		work,
	)

	gbot.AddRobot(robot)

	gbot.Start()
}

func move(motor *gpio.MotorDriver, wirevalue byte) {
	power := byte(motion.UnCenterizePower(wirevalue))
	fmt.Printf("motor ; data %d power %d\n", wirevalue, power)
	//fmt.Printf("motor left ; data uncenter %d\n", motion.UnCenterizePower(power))
	direction := motion.Direction(wirevalue)
	if direction == motion.Forward {
		fmt.Printf("motor ; forward %d\n", power)
		motor.Backward(0)
		motor.Forward(power)
	} else if direction == motion.Backward {
		fmt.Printf("motor ; backward%d\n", power)
		motor.Forward(0)
		motor.Backward(power)
	} else {
		fmt.Printf("motor off (power=%d)\n", power)
		motor.Off()
	}
}
