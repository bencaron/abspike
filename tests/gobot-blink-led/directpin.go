package main

import (
	"time"

	"github.com/hybridgroup/gobot"
	"github.com/hybridgroup/gobot/platforms/firmata"
	"github.com/hybridgroup/gobot/platforms/gpio"
)

func main() {
	gbot := gobot.NewGobot()
	firmataAdaptor := firmata.NewFirmataAdaptor("myFirmata", "/dev/ttyUSB0")
	pin := gpio.NewDirectPinDriver(firmataAdaptor, "pin", "13")
	work := func() {
		level := byte(1)

		gobot.Every(1*time.Second, func() {
			//pin.DigitalWrite(level)
			if level == 1 {
				level = 0
				pin.Off()
			} else {
				level = 1
				pin.On()
			}
		})
	}
	robot := gobot.NewRobot("bot",
		[]gobot.Connection{firmataAdaptor},
		[]gobot.Device{pin},
		work,
	)

	gbot.AddRobot(robot)

	gbot.Start()
}
