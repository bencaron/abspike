package main

import (
	"time"

	"github.com/hybridgroup/gobot"
	"github.com/hybridgroup/gobot/platforms/firmata"
	"github.com/hybridgroup/gobot/platforms/gpio"
)

func main() {
	gbot := gobot.NewGobot()
	firmataAdaptor := firmata.NewFirmataAdaptor("arduino", "/dev/ttyUSB0")
	led := gpio.NewLedDriver(firmataAdaptor, "led", "13")
	motorA := gpio.NewMotorDriver(firmataAdaptor, "motorA", "10")
	motorA.ForwardPin = "8"
	motorA.BackwardPin = "9"

	work := func() {
		direction := byte(1)

		gobot.Every(2*time.Second, func() {
			led.Toggle()
			motorA.Off()
			if direction == 1 {
				direction = 0
				motorA.Forward(200)
			} else {
				direction = 1
				motorA.Backward(200)
			}
		})
	}
	robot := gobot.NewRobot("bot",
		[]gobot.Connection{firmataAdaptor},
		[]gobot.Device{motorA},
		work,
	)

	gbot.AddRobot(robot)

	gbot.Start()
}
