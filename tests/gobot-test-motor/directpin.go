package main

import (
	"time"

	"github.com/hybridgroup/gobot"
	"github.com/hybridgroup/gobot/platforms/firmata"
	"github.com/hybridgroup/gobot/platforms/gpio"
)

func main() {
	gbot := gobot.NewGobot()
	firmataAdaptor := firmata.NewFirmataAdaptor("myFirmata", "/dev/ttyUSB0")
	led := gpio.NewDirectPinDriver(firmataAdaptor, "led", "13")
	enA := gpio.NewDirectPinDriver(firmataAdaptor, "enB", "10")
	in8 := gpio.NewDirectPinDriver(firmataAdaptor, "in8", "8")
	in9 := gpio.NewDirectPinDriver(firmataAdaptor, "in9", "9")
	//enB := gpio.NewDirectPinDriver(firmataAdaptor, "enB", "5")
	//in3 := gpio.NewDirectPinDriver(firmataAdaptor, "in3", "3")
	//in4 := gpio.NewDirectPinDriver(firmataAdaptor, "in4", "4")
	work := func() {
		direction := byte(1)
		speed := byte(255)

		gobot.Every(2*time.Second, func() {
			enA.DigitalWrite(direction)
			if direction == 1 {
				led.On()
				direction = 0
				enA.DigitalWrite(0)
				in8.Off()
				in9.On()
				enA.DigitalWrite(speed)
			} else {
				led.Off()
				direction = 1
				enA.DigitalWrite(0)
				in8.On()
				in9.Off()
				enA.DigitalWrite(speed)
			}
		})
	}
	robot := gobot.NewRobot("bot",
		[]gobot.Connection{firmataAdaptor},
		//[]gobot.Device{led, enB, in3, in4},
		[]gobot.Device{led, enA, in8, in9},
		work,
	)

	gbot.AddRobot(robot)

	gbot.Start()
}
